package ru.t1.gorodtsova.tm.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.t1.gorodtsova.tm")
public class ApplicationConfiguration {

}
