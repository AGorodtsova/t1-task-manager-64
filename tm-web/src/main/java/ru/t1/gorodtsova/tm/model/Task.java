package ru.t1.gorodtsova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1.gorodtsova.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class Task {

    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull
    private String name;

    @Nullable
    private String description;

    @NotNull
    private Status status = Status.NOT_STARTED;

    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date created = new Date();

    @Nullable
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateStart;

    @Nullable
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateFinish;

    @Nullable
    private String projectId;

    public Task(@NotNull final String name) {
        this.name = name;
    }

}
