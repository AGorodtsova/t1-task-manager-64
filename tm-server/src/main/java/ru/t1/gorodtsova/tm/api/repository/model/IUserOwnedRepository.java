package ru.t1.gorodtsova.tm.api.repository.model;

import ru.t1.gorodtsova.tm.model.AbstractUserOwnedModel;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

}