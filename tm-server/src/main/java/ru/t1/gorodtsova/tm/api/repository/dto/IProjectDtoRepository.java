package ru.t1.gorodtsova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.gorodtsova.tm.dto.model.ProjectDTO;

import java.util.List;

@Repository
public interface IProjectDtoRepository extends IUserOwnedDtoRepository<ProjectDTO> {

    @NotNull
    List<ProjectDTO> findByUserId(@NotNull String userId);

    @NotNull
    @Query("SELECT p FROM ProjectDTO p WHERE p.userId = :userId ORDER BY :sortColumn")
    List<ProjectDTO> findAllByUserIdWithSort(@NotNull @Param("userId") String userId, @NotNull @Param("sortColumn") String sortColumn);

    @Nullable
    ProjectDTO findOneByUserIdAndId(@NotNull String userId, @NotNull String id);

    void deleteByUserId(@NotNull String userId);

    void deleteOneByUserIdAndId(@NotNull String userId, @NotNull String id);

    boolean existsByUserIdAndId(@NotNull String userId, @NotNull String id);

    long countByUserId(@NotNull String userId);

}
