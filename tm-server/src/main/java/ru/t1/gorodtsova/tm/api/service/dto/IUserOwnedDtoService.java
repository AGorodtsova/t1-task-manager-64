package ru.t1.gorodtsova.tm.api.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.gorodtsova.tm.dto.model.AbstractUserOwnedModelDTO;

import java.util.List;

public interface IUserOwnedDtoService<M extends AbstractUserOwnedModelDTO> extends IDtoService<M> {

    @Transactional
    M add(@Nullable String userId, M model);

    @NotNull
    List<M> findAll(@Nullable String userId);

    M findOneById(@Nullable String userId, @Nullable String id);

    @Transactional
    void removeAll(@Nullable String userId);

    @Transactional
    void removeOne(@Nullable String userId, M model);

    @Transactional
    void removeOneById(@Nullable String userId, @Nullable String id);

    boolean existsById(@Nullable String userId, @Nullable String id);

    @SneakyThrows
    long getSize(@Nullable String userId);

}
