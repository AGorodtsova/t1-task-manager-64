package ru.t1.gorodtsova.tm.service.dto;

import ru.t1.gorodtsova.tm.api.service.dto.IUserOwnedDtoService;
import ru.t1.gorodtsova.tm.dto.model.AbstractUserOwnedModelDTO;

public abstract class AbstractUserOwnedDtoService
        <M extends AbstractUserOwnedModelDTO>
        extends AbstractDtoService<M>
        implements IUserOwnedDtoService<M> {

}
