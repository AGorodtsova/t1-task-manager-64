package ru.t1.gorodtsova.tm.api.repository.model;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.t1.gorodtsova.tm.model.AbstractModel;

public interface IRepository<M extends AbstractModel> extends JpaRepository<M, String> {

}