package ru.t1.gorodtsova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import ru.t1.gorodtsova.tm.api.service.dto.IProjectDtoService;
import ru.t1.gorodtsova.tm.api.service.dto.IProjectTaskDtoService;
import ru.t1.gorodtsova.tm.api.service.dto.ITaskDtoService;
import ru.t1.gorodtsova.tm.api.service.dto.IUserDtoService;
import ru.t1.gorodtsova.tm.configuration.ServerConfiguration;
import ru.t1.gorodtsova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.gorodtsova.tm.exception.entity.TaskNotFoundException;
import ru.t1.gorodtsova.tm.exception.field.ProjectIdEmptyException;
import ru.t1.gorodtsova.tm.exception.field.TaskIdEmptyException;
import ru.t1.gorodtsova.tm.exception.field.UserIdEmptyException;
import ru.t1.gorodtsova.tm.marker.UnitCategory;

import static ru.t1.gorodtsova.tm.constant.ProjectTestData.*;
import static ru.t1.gorodtsova.tm.constant.TaskTestData.*;
import static ru.t1.gorodtsova.tm.constant.UserTestData.USER1;
import static ru.t1.gorodtsova.tm.constant.UserTestData.USER_LIST;

@Category(UnitCategory.class)
public class ProjectTaskServiceTest {

    @NotNull
    private AbstractApplicationContext context;

    @NotNull
    private IUserDtoService userService;

    @NotNull
    private IProjectDtoService projectService;

    @NotNull
    private ITaskDtoService taskService;

    @NotNull
    private IProjectTaskDtoService projectTaskService;

    @Rule
    @NotNull
    public final ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() {
        context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        userService = context.getBean(IUserDtoService.class);
        projectService = context.getBean(IProjectDtoService.class);
        taskService = context.getBean(ITaskDtoService.class);
        projectTaskService = context.getBean(IProjectTaskDtoService.class);

        userService.add(USER_LIST);
        projectService.add(PROJECT_LIST);
        taskService.add(TASK_LIST);
    }

    @After
    public void tearDown() {
        taskService.removeAll();
        projectService.removeAll();
        userService.removeAll();
        context.close();
    }

    @Test
    public void bindTaskToProject() {
        projectTaskService.bindTaskToProject(USER1.getId(), USER1_PROJECT1.getId(), USER1_TASK1.getId());
        Assert.assertEquals(USER1_PROJECT1.getId(), taskService.findOneById(USER1_TASK1.getId()).getProjectId());
        Assert.assertNotEquals(USER1_PROJECT2.getId(), taskService.findOneById(USER1_TASK1.getId()).getProjectId());

        thrown.expect(UserIdEmptyException.class);
        projectTaskService.bindTaskToProject(null, USER1_PROJECT1.getId(), USER1_TASK1.getId());

        thrown.expect(ProjectIdEmptyException.class);
        projectTaskService.bindTaskToProject(USER1.getId(), null, USER1_TASK1.getId());

        thrown.expect(TaskIdEmptyException.class);
        projectTaskService.bindTaskToProject(USER1.getId(), USER1_PROJECT1.getId(), null);

        thrown.expect(ProjectNotFoundException.class);
        projectTaskService.bindTaskToProject(USER1.getId(), USER2_PROJECT1.getId(), USER1_TASK1.getId());

        thrown.expect(TaskNotFoundException.class);
        projectTaskService.bindTaskToProject(USER1.getId(), USER1_PROJECT1.getId(), USER2_TASK1.getId());
    }

    @Test
    public void unbindTaskFromProject() {
        projectTaskService.bindTaskToProject(USER1.getId(), USER1_PROJECT1.getId(), USER1_TASK1.getId());
        Assert.assertNotNull(taskService.findOneById(USER1_TASK1.getId()).getProjectId());
        projectTaskService.unbindTaskFromProject(USER1.getId(), USER1_PROJECT1.getId(), USER1_TASK1.getId());
        Assert.assertNull(taskService.findOneById(USER1_TASK1.getId()).getProjectId());

        thrown.expect(UserIdEmptyException.class);
        projectTaskService.unbindTaskFromProject(null, USER1_PROJECT1.getId(), USER1_TASK1.getId());

        thrown.expect(ProjectIdEmptyException.class);
        projectTaskService.unbindTaskFromProject(USER1.getId(), null, USER1_TASK1.getId());

        thrown.expect(TaskIdEmptyException.class);
        projectTaskService.unbindTaskFromProject(USER1.getId(), USER1_PROJECT1.getId(), null);

        thrown.expect(ProjectNotFoundException.class);
        projectTaskService.unbindTaskFromProject(USER1.getId(), USER2_PROJECT1.getId(), USER1_TASK1.getId());

        thrown.expect(TaskNotFoundException.class);
        projectTaskService.unbindTaskFromProject(USER1.getId(), USER1_PROJECT1.getId(), USER2_TASK1.getId());
    }

    @Test
    public void removeProjectById() {
        projectTaskService.bindTaskToProject(USER1.getId(), USER1_PROJECT1.getId(), USER1_TASK1.getId());
        projectTaskService.bindTaskToProject(USER1.getId(), USER1_PROJECT1.getId(), USER1_TASK2.getId());
        projectTaskService.removeProjectById(USER1.getId(), USER1_PROJECT1.getId());
        Assert.assertFalse(projectService.existsById(USER1_PROJECT1.getId()));
        Assert.assertFalse(taskService.existsById(USER1_TASK1.getId()));
        Assert.assertFalse(taskService.existsById(USER1_TASK2.getId()));

        thrown.expect(UserIdEmptyException.class);
        projectTaskService.removeProjectById(null, USER1_PROJECT1.getId());

        thrown.expect(ProjectIdEmptyException.class);
        projectTaskService.removeProjectById(USER1.getId(), null);

        thrown.expect(ProjectNotFoundException.class);
        projectTaskService.removeProjectById(USER1.getId(), USER2_PROJECT1.getId());
    }

}
