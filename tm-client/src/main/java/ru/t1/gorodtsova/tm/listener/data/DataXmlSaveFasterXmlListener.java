package ru.t1.gorodtsova.tm.listener.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.gorodtsova.tm.dto.request.domain.DataXmlSaveFasterXmlRequest;
import ru.t1.gorodtsova.tm.event.ConsoleEvent;

@Component
public final class DataXmlSaveFasterXmlListener extends AbstractDataListener {

    @NotNull
    private final String DESCRIPTION = "Save data in xml file";

    @NotNull
    private final String NAME = "data-save-xml";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@dataXmlSaveFasterXmlListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[DATA SAVE XML]");
        @NotNull final DataXmlSaveFasterXmlRequest request = new DataXmlSaveFasterXmlRequest(getToken());
        domainEndpoint.saveDataXmlFasterXml(request);
    }

}
