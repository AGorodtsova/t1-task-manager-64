package ru.t1.gorodtsova.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.gorodtsova.tm.event.ConsoleEvent;
import ru.t1.gorodtsova.tm.util.FormatUtil;

@Component
public final class SystemInfoListener extends AbstractSystemListener {

    @NotNull
    private final String ARGUMENT = "-i";

    @NotNull
    private final String DESCRIPTION = "Show system information";

    @NotNull
    private final String NAME = "info";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@systemInfoListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[INFO]");
        final long availableProcessors = Runtime.getRuntime().availableProcessors();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long usedMemory = totalMemory - freeMemory;

        @NotNull final String freeMemoryFormat = FormatUtil.formatBytes(freeMemory);
        @NotNull final String totalMemoryFormat = FormatUtil.formatBytes(totalMemory);
        @NotNull final String usedMemoryFormat = FormatUtil.formatBytes(usedMemory);
        @NotNull final String maxMemoryFormat = FormatUtil.formatBytes(maxMemory);
        @NotNull final String maxMemoryValue = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryFormat;

        System.out.println("Available processors: " + availableProcessors);
        System.out.println("Free memory: " + freeMemoryFormat);
        System.out.println("Maximum memory: " + maxMemoryValue);
        System.out.println("Total memory: " + totalMemoryFormat);
        System.out.println("Used memory: " + usedMemoryFormat);
    }

}
