package ru.t1.gorodtsova.tm.service;

import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.gorodtsova.tm.api.IPropertyService;

import java.util.Properties;

@Getter
@Service
@PropertySource("classpath:application.properties")
public final class PropertyService implements IPropertyService {

    @Value("#{environment['activemq.host']}")
    private String brokerUrl;

    @Value("#{environment['mongo.port']}")
    private Integer mongoPort;

    @Value("#{environment['mongo.host']}")
    private String mongoHost;

}
