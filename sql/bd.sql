-- Database: tm

-- DROP DATABASE IF EXISTS tm;

CREATE DATABASE tm
    WITH
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'C'
    LC_CTYPE = 'C'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

COMMENT ON DATABASE tm
    IS 'TASK MANAGER';